/// Lets do some documentation - Main Method
/// *main* method
/// *inputs:*
/// - not real
/// *outputs:*
/// - ran good or not 0 or 1
/// no /tilde/ sadly
///
fn main() {
    println!("Hello, world!");
    println!("{}, string interpolation is {}", "Magical", "Amazin");
    println!("{:?}", [1, 2, 3]);
    println!("{:#?}", [1, 2, 3]);
    let x = format!("{}, {}!", "Hello", "world");
    println!("{}", x);
    let _a = true;
    let _b: bool = true;
    let (_x, _y) = (1, 2);
    let mut _z = 5;
    _z = 6;
    const NY: i32 = 5;
    static NZ: i32 = 5;
    let apple = NY + NZ;
    println!("yeet {}", apple);
    fn print_sum(a: i8, b: i8) {
        println!("sum is: {}", a + b);
    }
    let _sum = print_sum(1, 2);
    fn ret_plus_two(a: i32) -> i32 {
        a + 2 // implied return
    }
    let new_sum = ret_plus_two(40);
    println!("new_sum: {}", new_sum);
    let b = ret_plus_two; // function pointers as Data Types
    let c = b(5);
    println!("c: {}", c);
    let b2: fn(i32) -> i32 = ret_plus_two; // with type declaration
    let c2 = b2(5);
    println!("c2: {}", c2);

    // lets do an anonymouse function
    // which rustaceans call 'closures'
    fn _get_square_value(x: i32) -> i32 {
        x * x
    }
    // is equivalent to
    let square = |x: i32| -> i32 { x * x };
    // also equivalent to
    // NOT WORKIN outside of simple case let square2 = |x| x * x;
    let x_bool = true;
    let y_bool: bool = false;
    println!("x: {} y: {}", x_bool, y_bool);
    // no TRUE, FALSE, 1, 0
    //
    // CHARS
    let x_ch = 'x';
    let y_ch = '😎';
    // take-away, a char is not 1 byte :(
    // a char in rust is 4 bytes. Fudge unicode.
    //
    // Integers: i8,16,32,64,128
    // min int : max int -> -(2^(n-1) : 2^(n-1)-1
    // i8 -> -128 : 127
    // i16 -> -32768 : 32767
    // i32 -> -2147483648 : 2147483647
    // blah blah blah....
    //
    // UNSIGNED integers:
    // u8,16,32,64,128
    // u8 -> 0:255
    // u16 -> 0:65535
    // u32 -> 0:4294967295
    // u128 -> 0:340282366920938463463374607431768211455
    // Yes, I copy pasted that last one.
    // You can use min_value() and max_value() functions on all ints.
    //
    // POINTERS:
    // Pointer sizes isize, usize will return size depending on comp arch
    // 32bit OR 64bit ;D
    //
    // Floating Points FLOATZ
    // f32, f64 -> single precision, Double precision
    // this is the same as Float() and Double() in other langauges.
    //
    // ARRAYS
    let arr = [1, 2, 3];
    let mut arr2 = [1, 2, 3];
    let arr3: [i32; 0] = []; // empty
    let arr4: [i32; 3] = [1, 2, 3]; // Wutz the pt of this
    let e = ["my val"; 3]; // ["my val", "my val", "my val"] repeats 3 timez
    println!("{:?}", arr); // [1, 2, 3]
                           // change to {:#?} and itll  print with new linez
                           // [
                           //   1,
                           //   2,
                           //   3,
                           // ]
                           //
                           // TUPLES:
                           // Fixed sized, ordered, multiple types
    let atuple = (1, 1.5, true, 'a', "Hello, world!");
    // access with dot accessor
    println!("5th item in tuple: {}", atuple.4);
    let b: (i32, f64) = (1, 1.5); // u can specify typesize
    let (c, d) = b; // u can destructure tuples
    let (e, _, _, _, f) = atuple; // and ignore ones u dont care about
    let gt = (0,); // single element tuple
    let ht = (b, (2, 4), 5); // tuple-ception
    println!("tuplez: {:?}", atuple);
    // NOTE: Tuples are immutable, with mut keyword the element
    // count cannot be changed, but if u change an el u need to
    // change to the same data type of prev value
    //
    // SLICE - Dynamically sized reference to another data struct
    let parentarr: [i32; 4] = [1, 2, 3, 4];
    let barr: &[i32] = &parentarr; // slicing whole array
    let carr = &parentarr[0..4]; // from 0 to 4th excluding
    let darr = &parentarr[..]; // slicing whole array
    let earr = &parentarr[1..3]; // [2, 3]
                                 //                   [1..]   -> [2, 3, 4]
                                 //                   [..3]   -> [1, 2, 3]
                                 //
                                 // STR - unsized UTF-8 seq of Unicode string slices
    let a = "Hello, world"; // a: &'static str
    let b: &str = "... Im not going to write uunicode screw that";
    // Its an immutable statically allocated slice holding an unknown sized seq of UTF-8
    // stored somewhere in mem. &str is used to borrow and assign the whole array
    // to a given variable binding.
    // Use String when needing Ownership (We learn dis later)
    // Use &str when needing to borrow a string
    //
    // FUNCTIONS
    // Skipping
    //
    // OPERATORS
    // Skipping
    //
    // COMPARISON OPERATORS
    // Skip
    //
    // NOTE: true > false, almost aas if true is 1 and false is 0
    //       'a' > 'A' , almost as if though ascii map has capital letters defined first.
    //
    // LOGICAL OPERATORS
    // ! && and ||
    // NOTE: ! can be used to flip individual bits, but must be 2s complement form
    //       ex.
    let LOa = !-2; // 1  -> 2s complement form of -2 is 1110, flipped is 0001 == 1
                   //        !-1     0  -> 1111 flipped is 0000 = 0;
                   //        !0     -1
                   //        !1     -2  -> 0001 already in 2s comp, flipped -> 1110, which is -2, since 1111 is -1
                   //        Oh, reminds me of school again
                   //
    println!("!-2: does twos complement bit inversion {}", LOa);
    //
    // BITWISE Operators
    // skip lul
    //
    // ASSIGNMENT
    let mut ass = 2;
    ass += 5; // 2 + 5
    ass -= 2; // 7 - 2
    ass *= 5; // kill me
    ass /= 2; // 25/2 = 12 , NO mantisssssa
    ass %= 5; // 12%5 = 2
              // NOTE: These are the cool bitwise onez
    ass &= 2; // 10 && 10 -> 10 -> 2
    ass |= 5; // 010 || 101 -> 111 -> 7
    ass ^= 2; // 111 != 010 -> 101 -> 5
    ass <<= 1; // '101' + '9' -> 1010 -> 10 // same as multiply by 2
    ass >>= 2; // '1010' move over twice -> 0010 -> 2 // same as divide by 4
               // All of this is old news, ancient boring, for the most part...
               // hopefully the /fun/ comes soonTm
               //
               // TYPE CASTING
    let aTC = 15;
    let bTC = (aTC as f64) / 2.0; // 7.5
                                  //
                                  // BORROWING AND DEREFERENCE OPERATORS
                                  // & or &mut <- for borrowing
                                  // * <- for dereferencing
                                  //
                                  // CONTROL FLOWS aka CONDITIONALS
    let team_size = 7;
    if team_size < 5 {
        println!("Smaall");
    } else if team_size < 10 {
        println!("Medium");
    } else {
        println!("Your team 2 fuggin yooooge");
    }

    //optimistic code?
    let team_size = 7;
    let team_size_in_text = if team_size < 5 {
        "Smaalll"
    } else if team_size < 10 {
        "Medium"
    } else {
        "Large"
    };
    println!("Cuurrent teaam size: {}", team_size_in_text);

    println!("assignment: {}", ass);

    println!("done.");
}
